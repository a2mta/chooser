import * as THREE from 'three';
import Stats from 'three/examples/jsm/libs/stats.module';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { SavePass } from 'three/examples/jsm/postprocessing/SavePass';
import { BlendShader } from 'three/examples/jsm/shaders/BlendShader';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';

export default class SceneInit {
  constructor(canvasId) {
    // NOTE: Core components to initialize Three.js app.
    this.scene = undefined;
    this.camera = undefined;
    this.renderer = undefined;

    // NOTE: Camera params;
    this.fov = 45;
    this.nearPlane = 1;
    this.farPlane = 1000;
    this.canvasId = canvasId;

    // NOTE: Additional components.
    this.clock = undefined;
    this.stats = undefined;
    this.controls = undefined;

    // NOTE: Lighting is basically required.
    this.spotLight = undefined;
    this.ambientLight = undefined;
  }

  initialize() {
    if (typeof window !== 'undefined') {
      this.scene = new THREE.Scene();
      console.info(this.scene);
      this.scene.background = new THREE.Color('#DC35DA');
      this.camera = new THREE.PerspectiveCamera(
        this.fov,
        window.innerWidth / window.innerHeight,
        1,
        1000
      );
      // this.camera.position.z = 2;
      // this.camera.rotation.z = 1;
      this.scene.fog = new THREE.Fog(0xcccccc, 1, 100);
      // const gui = new GUI();

      // const guiFolder = gui.addFolder('Camera');
      // guiFolder.add(this.camera.rotation, 'x', 0, Math.PI * 2);
      // guiFolder.add(this.camera.rotation, 'y', 0, Math.PI * 2);
      // guiFolder.add(this.camera.rotation, 'z', 0, Math.PI * 2);

      // // guiFolder.add(gltfScene.scene.position, 'x', -100, 100);
      // // guiFolder.add(gltfScene.scene.position, 'y', -100, 100);

      // guiFolder.open();

      // NOTE: Specify a canvas which is already created in the HTML.
      const canvas = document.getElementById(this.canvasId);

      this.renderer = new THREE.WebGLRenderer({
        canvas,
        antialias: true,
      });

      this.renderer.setSize(window.innerWidth, window.innerHeight);

      document.body.appendChild(this.renderer.domElement);

      this.stats = Stats();
      document.body.appendChild(this.stats.dom);

      // ambient light which is for the whole scene
      this.ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
      this.ambientLight.castShadow = true;
      this.scene.add(this.ambientLight);

      // spot light which is illuminating the chart directly
      this.spotLight = new THREE.SpotLight(0xffffff, 1);
      this.spotLight.castShadow = true;
      this.spotLight.position.set(0, 64, 32);
      this.scene.add(this.spotLight);

      // if window resizes
      window.addEventListener('resize', () => this.onWindowResize(), false);

      // NOTE: Load space background.
      // this.loader = new THREE.TextureLoader();
      // this.scene.background = this.loader.load('./pics/space.jpeg');

      this.composer = new EffectComposer(this.renderer);

      // render pass
      const renderPass = new RenderPass(this.scene, this.camera);

      const renderTargetParameters = {
        minFilter: THREE.LinearFilter,
        magFilter: THREE.LinearFilter,
        stencilBuffer: false,
      };

      // save pass
      const savePass = new SavePass(
        new THREE.WebGLRenderTarget(
          canvas.clientWidth,
          canvas.clientHeight,
          renderTargetParameters
        )
      );

      // blend pass
      const blendPass = new ShaderPass(BlendShader, 'tDiffuse1');
      blendPass.uniforms['tDiffuse2'].value = savePass.renderTarget.texture;
      blendPass.uniforms['mixRatio'].value = 0.83;

      const bloomPass = new UnrealBloomPass(
        new THREE.Vector2(window.innerWidth, window.innerHeight),
        0.2,
        0.1,
        0.1
      );

      // output pass
      const outputPass = new ShaderPass(CopyShader);
      outputPass.renderToScreen = true;

      // adding passes to composer
      this.composer.addPass(renderPass);
      this.composer.addPass(bloomPass);
      this.composer.addPass(blendPass);
      this.composer.addPass(savePass);
      this.composer.addPass(outputPass);
    }
  }

  animate() {
    window.requestAnimationFrame(this.animate.bind(this));
    this.render();
    this.stats.update();
  }

  render() {
    this.composer.render();
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }
}
