import dynamic from 'next/dynamic'

const ComponentWithNoSSR = dynamic(
  () => import('./page'),
  { ssr: false }
)

function Hello() {
    return (
      <><ComponentWithNoSSR/></>
    )
  }
  
  export default Hello
  