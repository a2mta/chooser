import * as THREE from 'three';
import { useCallback, useEffect, useRef, useState } from 'react';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Font, FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import SceneInit from '../common/Scene';

const names = [
  'Dima J',
  'Dima G',
  'Dima E',
  'Vlad',
  'Alexey',
  'Oleg',
  'Yulia',
  'Askhat',
];

var axis = new THREE.Vector3(0, 1, 0); //tilted a bit on x and y - feel free to plug your different axis here

const color = 0x00ff00;
const matDark = new THREE.LineBasicMaterial({
  color,
  side: THREE.DoubleSide,
});

const loadStatue = (
  loader: GLTFLoader,
  scene: THREE.Scene,
  pos: 'left' | 'right'
) => {
  loader.load('./mod.glb', (gltfScene) => {
    const isLeft = pos === 'left';
    gltfScene.scene.rotation.x = 0;
    gltfScene.scene.rotation.y = 0;
    gltfScene.scene.rotation.z = isLeft ? -0.9 : 0.9;
    gltfScene.scene.position.y = -20;
    gltfScene.scene.position.x = isLeft ? -38 : 38;
    gltfScene.scene.position.z = -50;
    gltfScene.scene.scale.set(12, 12, 12);
    scene?.add(gltfScene.scene);
    const scale = new THREE.Vector3(1, 1, 1);
    if (isLeft) {
      scale.x *= -1;
    }
    gltfScene.scene.scale.multiply(scale);

    const animate = () => {
      if (gltfScene) {
        gltfScene.scene.rotateOnAxis(axis, isLeft ? -0.01 : 0.01);
      }
      requestAnimationFrame(animate);
    };
    animate();
  });
};

export default function Home() {
  const canvasScene = useRef<SceneInit>();
  const [font, setFont] = useState<Font>();
  const [message, setMessage] = useState('Let random\n   decide');
  const [rotating, setRotate] = useState(false);
  const textMesh = canvasScene.current?.scene?.getObjectByName('random name');
  const animationRef = useRef(-1);

  const drawText = () => {
    const prevText = canvasScene.current?.scene?.getObjectByName('random name');
    const shapes = font?.generateShapes(message, 100);

    const geometry = new THREE.ShapeGeometry(shapes);

    geometry.computeBoundingBox();

    const xMid =
      -0.5 * (geometry.boundingBox!.max.x - geometry.boundingBox!.min.x);

    geometry.translate(xMid, 0, 0);

    if (prevText) {
      canvasScene.current?.scene?.remove(prevText);
    }

    const text = new THREE.Mesh(geometry, matDark);
    text.name = 'random name';
    text.position.z = -120;
    text.position.x = 0;
    text.position.y = 20;
    text.scale.y = 0.1;
    text.scale.x = 0.1;
    canvasScene.current?.scene?.add(text);
  };

  const loop = useCallback((textMesh: any, rotating: boolean) => {
    if (textMesh) {
      if (!rotating) {
        cancelAnimationFrame(animationRef.current);

        return;
      }
      textMesh.rotateOnAxis(axis, 0.1);
      animationRef.current = requestAnimationFrame(() =>
        loop(textMesh, rotating)
      );
    }
  }, []);

  useEffect(() => {
    const songElement = document.getElementById('song') as HTMLAudioElement;
    songElement.volume = 0.25
    if (rotating) {
      songElement?.play();
    } else {
      if (textMesh) {
        textMesh.rotation.y = 0;
      }
    }
    loop(textMesh, rotating);
  }, [rotating, loop]);

  useEffect(() => {
    if (font) {
      drawText();
    } else {
      const loader = new FontLoader();
      if (!font) {
        loader.load('optimer_bold.typeface.json', function (font) {
          setFont(font);
        });
      }
    }
  }, [font, message]);

  const handleRotate = () => {
    setRotate(true);
    setTimeout(
      () => setMessage(names[Math.floor(Math.random() * names.length - 1)]),
      5000
    );
    setTimeout(() => setRotate(false), 6500);
  };

  useEffect(() => {
    // let canvasScene: SceneInit;
    if (typeof window !== 'undefined') {
      canvasScene.current = new SceneInit('myThreeJsCanvas');
      canvasScene.current?.initialize();

      const glftLoader = new GLTFLoader();

      loadStatue(glftLoader, canvasScene.current?.scene!, 'right');
      loadStatue(glftLoader, canvasScene.current?.scene!, 'left');

      canvasScene.current.animate();
      // const spector = new SPECTOR.Spector();
      const canvas = document.getElementById('myThreeJsCanvas');
      // spector.spyCanvases();
      // spector.displayUI();

      canvas?.addEventListener('click', handleRotate);
    }
  }, []);

  return (
    <>
      <audio id='song' autoPlay preload='auto'>
        <source src='speen.mp3' type='audio/mpeg' />
      </audio>
      <canvas id='myThreeJsCanvas' />
    </>
  );
}
